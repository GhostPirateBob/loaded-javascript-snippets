# loaded-javascript-snippets

Collection of utility and helper snippets for writing JavaScript in VS Code.

Already in WordPress compatibility/no-conflict mode! 🤣

To build the extension use the Visual Studio Code Extensions package.

`npm install -g vsce`

Then package the extension into a .vsix file from the project folder.

`vsce package`

Enter 'jq' to get a list of available jQuery functions

Enter 'vj' to get a list of available Vanilla JavaScript functions

# index

jqAfter               Insert content after each element in the elements selected.

jqAjax                Perform an asynchronous HTTP (Ajax) request.

jqAppend              Insert content to the end of each element in the elements selected.

jqAppendTo            Insert every element in the elements selected to the end of the target.

jqAttrGet             Get the value of an attribute for the first element selected.

jqAttrRemove          Remove an attribute from each element in the elements selected.

jqAttrSet             Set one or more attributes for the elements selected.

jqBefore              Insert content before each element in the elements selected.

jqBind                Attach a handler to an event for the elements.

jqBlur                Bind an event handler to the "blur" event$ or trigger that event.

jqChange              Bind an event handler to the "change" event$ or trigger that event.

jqClassAdd            Adds the specified class(es) to each of the elements selected.

jqClassRemove         Remove a single class$ multiple classes$ or all classes from each element in the elements selected.

jqClassToggle         Add or remove one or more classes from each element in the set$ depending on its presence.

jqClick               Bind an event handler to the "click" event$ or trigger that event.

jqClone               Create a deep copy of the elements selected.

jqCloneEvents         Create a deep copy (including events) of the elements selected.

jqCssGet              Get the computed style properties for the first element selected.

jqCssSet              Set one or more CSS properties for the elements selected.

jqDataGet             Return the value at the named data store for the first element.

jqDataRemove          Remove a previously-stored piece of data.

jqDataSet             Store arbitrary data associated with the matched elements.

jqDie                 Remove an event handler previously attached using .live() from the elements.

jqDieAll              Remove all event handlers previously attached using .live() from the elements.

jqEmpty               Remove all child nodes of the element from the DOM.

jqFadeIn              Display the element by fading to opaque.

jqFadeInThen          Display the element by fading to opaque$ then run a function.

jqFadeOut             Hide the element by fading to transparent.

jqFadeOutThen         Hide the element by fading to transparent$ then run a function.

jqFadeTo              Adjust the opacity of the element.

jqFadeToThen          Adjust the opacity of the element$ then run a function.

jqFind                Get the descendants of each element$ filtered by a selector.

jqFocus               Bind an event handler to the "focus" event$ or trigger that event.

jqForEach             Select each element of a class or other item for further processing.

jqGet                 Load data from the server using a HTTP GET request.

jqGetJson             Load JSON-encoded data from the server using a GET HTTP request.

jqGetScript           Load a JavaScript file from URL$ then execute it.

jqHasClass            Determine whether any of the matched elements are assigned the given class.

jqHeightGet           Get the current computed height for the first element.

jqHeightSet           Set the CSS height of every matched element.

jqHide                Hide the selected elements$ sets inline CSS to "display: none;"

jqHideThen            Hide the matched elements.

jqHover               Bind two handlers to the element$ to be run on mouseover and mouseout.

jqHtmlGet             Get the HTML contents of the first element selected.

jqHtmlSet             Set the HTML contents of each element in the elements selected.

jqInnerHeight         Get the current height (px) for the first element selected$ includes padding but not border.

jqInnerWidth          Get the current inner width (px) for the first element selected$ includes padding but not border.

jqInsertAfter         Insert every element in the elements selected after the target.

jqInsertBefore        Insert every element in the elements selected before the target.

jqKeyDown             Bind an event handler to the "keydown" event$ or trigger that event.

jqKeyPress            Bind an event handler to the "keypress" event$ or trigger that event.

jqKeyUp               Bind an event handler to the "keyup" event$ or trigger that event.

jqLoad                Load data from URL and place the returned HTML into the matched element.

jqMap                 Translate all items in an array or object to new array of items.

jqMouseDown           Bind an event handler to the "mousedown" event$ or trigger that event.

jqMouseEnter          Bind an event handler to be fired when the mouse enters an element$ or trigger that handler.

jqMouseLeave          Bind an event handler to be fired when the mouse leaves an element$ or trigger that handler.

jqMouseMove           Bind an event handler to the "mousemove" event$ or trigger that event.

jqMouseOut            Bind an event handler to the "mouseout" event$ or trigger that event.

jqMouseOver           Bind an event handler to the "mouseover" event$ or trigger that event.

jqMouseUp             Bind an event handler to the "mouseup" event$ or trigger that event.

jqOffset              Get the current coordinates of the element selected$ relative to the document.

jqOn                  Attach an event handler to the selected element for a specific event.

jqOne                 Attach an event handler to the selected element for a specific event that can only run once.

jqOuterHeight         Get the current height (px) for the element$ includes padding$ border$ and (optionally with "true") margin.

jqOuterWidth          Get the current width height (px) for the element$ includes padding$ border$ and (optionally with "true") margin.

jqPosition            Get the current coordinates of the element$ relative to the parent.

jqPost                Load data from URL using an HTTP POST request.

jqPrepend             Insert content to the beginning of each element in the elements selected.

jqPrependTo           Insert every element in the elements selected to the beginning of the target.

jqReady               General use unction to run some code when the page has fully loaded.

jqRemove              Remove the elements selected from the DOM.

jqReplaceAll          Replace each target element with the elements selected.

jqResize              Bind an event handler to the "resize" event$ or trigger that event.

jqScroll              Bind an event handler to the "scroll" event$ or trigger that event.

jqScrollLeftGet       Get the current horizontal position of the scroll bar for the element.

jqScrollLeftSet       Set the current horizontal position of the scroll bar for each of the elements selected.

jqScrollTopGet        Get the current vertical position of the scroll bar for the first element selected.

jqScrollTopSet        Set the current vertical position of the scroll bar for each of the elements selected.

jqSelect              Bind an event handler to the select event.

jqSelectTrigger       Trigger the select event on and element.

jqShow                Display the matched elements by removing inline display: none;'s.

jqShowThen            Display the matched elements then run a function.

jqSubmit              Bind an event handler to the submit event.

jqSubmitTrigger       Trigger the "submit" event$ typically on a form.

jqTextGet             Get the text content of the element selected$ including all descendants.

jqTextSet             Set the text content of each element selected.

jqToggle              Display or hide the matched elements.

jqToggleThen          Display or hide the matched elements.

jqTriggerEvent        Trigger a specific event handler attached to the matched elements.

jqUnbind              Remove an attached event handler from the elements.

jqUnbindAll           Remove all event handlers from the elements.

jqUnload              Bind an event handler to the unload event.

jqValGet              Get the current value of the element$ typically form <input> fields.

jqValSet              Set the value of each element in the elements selected.

jqWidthGet            Get the current width (px) for the first element selected.

jqWidthSet            Set the CSS width (px) of each element in the elements selected.


## Sources/Credits

[Github: Don Jayamanne](https://github.com/DonJayamanne/jquerysnippets)


[Visual Studio 2015 - jQuery Code Snippets Extension](https://github.com/kspearrin/Visual-Studio-jQuery-Code-Snippets)

        
## License

[MIT](https://raw.githubusercontent.com/DonJayamanne/jquerysnippets/master/LICENSE)
